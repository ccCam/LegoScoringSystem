#include "newgroup.h"
#include "ui_newgroup.h"
#include <string>
#include <iostream>

NewGroup::NewGroup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewGroup)
{
    ui->setupUi(this);
}

NewGroup::~NewGroup()
{
    delete ui;
}

int NewGroup::brickCount()
{
    if (ui->radioButton_100->isChecked())
    {
        return 20000;
    }
    else if (ui->radioButton_110->isChecked())
    {
        return 18000;
    }
    else if (ui->radioButton_120->isChecked())
    {
        return 16000;
    }
    else if (ui->radioButton_130->isChecked())
    {
        return 15000;
    }
    else if (ui->radioButton_140->isChecked())
    {
        return 14000;
    }
    else if (ui->radioButton_150->isChecked())
    {
        return 13000;
    }
    else if (ui->radioButton_160->isChecked())
    {
        return 12000;
    }
    else if (ui->radioButton_170->isChecked())
    {
        return 10000;
    }
    else if (ui->radioButton_180->isChecked())
    {
        return 8000;
    }
    else if (ui->radioButton_190->isChecked())
    {
        return 6000;
    }
    else if (ui->radioButton_200->isChecked())
    {
        return 4000;
    }
    else
    {
        return 0;
    }
}

int NewGroup::height()
{
    if (ui->radioButton_20m->isChecked())
    {
        return 30000;
    }
    else if (ui->radioButton_19m->isChecked())
    {
        return 29000;
    }
    else if (ui->radioButton_18m->isChecked())
    {
        return 28000;
    }
    else if (ui->radioButton_17m->isChecked())
    {
        return 27000;
    }
    else if (ui->radioButton_16m->isChecked())
    {
        return 26000;
    }
    else if (ui->radioButton_15m->isChecked())
    {
        return 25000;
    }
    else if (ui->radioButton_14m->isChecked())
    {
        return 24000;
    }
    else if (ui->radioButton_13m->isChecked())
    {
        return 23000;
    }
    else if (ui->radioButton_12m->isChecked())
    {
        return 22000;
    }
    else if (ui->radioButton_11m->isChecked())
    {
        return 21000;
    }
    else if (ui->radioButton_10m->isChecked())
    {
        return 20000;
    }
    else if (ui->radioButton_05m->isChecked())
    {
        return 10000;
    }
    else if (ui->radioButton_00m->isChecked())
    {
        return 0;
    }
    else
    {
        return 0;
    }
}


void NewGroup::on_pushButton_Add_clicked()
{
    int brickAmount = brickCount();
    int heightAmount = height();


    std::string brickAmountString = std::to_string(heightAmount);
    QString brickAmountQString = QString::fromStdString(brickAmountString);
    ui->pushButton_Add->setText(brickAmountQString);


}

void NewGroup::on_pushButton_Cancel_clicked()
{
    QDialog::done(0);
    std::cout << "Ended" << std::endl;
}
